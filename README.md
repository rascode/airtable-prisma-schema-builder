# Airtable Prisma Schema Builder

The Airtable Prisma schema builder takes an Airtable Base as input and outputs a Prisma Schema file that can be used to create a Prisma migration and Prisma Client.